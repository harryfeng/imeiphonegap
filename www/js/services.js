'use strict';

/* Services */
var innoModule = angular.module('app.services', []);

innoModule.factory('testService', ['$window',
function(c) {
    $window.AV.initialize("fhcjp6jb2ukop6wq9wo5jzeto60oir5koxm155vs5rq8aglj", "6c2ti7h839xrsxbhpfhpbbz6859qh65vlknx478jnnsey8dz");

    $window.AV.getProductOrder = function(callback) {
        var TestProducts = $window.AV.Object.extend("productOrders");
        var query = new $window.AV.Query(TestProducts);
        query.include('Products')
        query.find({
            success : function(data) {
                callback(data);
            }
        });
    };

    $window.AV.changeOrderStatus = function(orderObj) {
        orderObj.set('orderStatus','hello');
        orderObj.save();
    };

    return $window.AV;
}]);
