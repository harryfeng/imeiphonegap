app.controller('homeCtrl', ['$scope', 'testService',
function($scope, testService) {
    testService.getProductOrder(function(data) {
        $scope.productsList = data;
        $scope.$apply();
    });
    
    $scope.changeStatus = function(item){
        testService.changeOrderStatus(item);
    }
    
    $scope.harry = 'home';

}]);
