 'use strict';

//boosi.声明AngularJS模块, 并把ui-router传入AngularJS主模块，所有的结合起来我们就得到了Angular模块
var app = angular.module('app', ['ngStorage', 'ui.router','ui.load','app.services', 'app.controllers']).run(['$rootScope', '$state', '$stateParams',
//这一行声明了把  $stateParams  和 $rootScope, $state,路由引擎作为函数参数传入，这样我们就可以为这个应用程序配置路由了.
function($rootScope, $state, $stateParams) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
}]).config(['$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide',
function($stateProvider, $urlRouterProvider, $controllerProvider, $compileProvider, $filterProvider, $provide) {

    // lazy controller, directive and service
    app.controller = $controllerProvider.register;
    app.directive = $compileProvider.directive;
    app.filter = $filterProvider.register;
    app.factory = $provide.factory;
    app.service = $provide.service;
    app.constant = $provide.constant;
    app.value = $provide.value;
    //整个网站首先从这里开始，这句话的意思是：我看看url是什么，如果url跟下面的都不对，我就加载dashboard－v1这一个页面
    //boosi  好，那这一行做什么的呢，如果没有路由引擎能匹配当前的导航状态，那它就会默认将路径路由至 index.html, 
    //这个页面就是状态名称被声明的地方. 只要理解了这个，那它就像switch case语句中的default选项.
    $urlRouterProvider.otherwise('/imeimodel/index');
    //这句是：如果url里面有imei，那我就先加载imei.html这个文件，然后你打开app.html这个页面

    $stateProvider.state('imei', {
        abstract : true,
        url : '/imei',
        templateUrl : 'tpl/app.html'
    })

        .state('imei.imei_img_particulars', {
		url : '/imei_img_particulars/:id',
		templateUrl : 'js/app/imei_img_particulars/imei_img_particulars.html'
		
    }).state('imei.imei_select_type', {
        url : '/imei_select_type/:id',
        templateUrl : 'js/app/imei_select_type/imei_select_type.html'
        // resolve : {
            // deps : ['uiLoad',
            // function(uiLoad) {
                // return uiLoad.load(['js/app/select_type/tu5.css']);
            // }]
        // }
        	
    }).state('imei.imei_recordPopup', {
        url : '/imei_recordPopup',
        templateUrl : 'js/app/imei_recordPopup/imei_recordPopup.html'
    }).state('imei.imei_setup', {
        url : '/imei_setup',
        templateUrl : 'js/app/imei_setup/imei_setup.html'
        // resolve : {
            // deps : ['uiLoad',
            // function(uiLoad) {
                // return uiLoad.load(['js/app/setup11/tu11_15.css']);
            // }]
        // }
    }).state('imei.imei_invite_friend', {
        url : '/imei_invite_friend',
        templateUrl : 'js/app/imei_invite_friend/imei_invite_friend.html',
        // resolve : {
            // deps : ['uiLoad',
            // function(uiLoad) {
                // return uiLoad.load(['js/app/login_interface13/tu11_15.css']);
            // }]
        // }
     }).state('imei.imei_login_interface', {
        url : '/imei_login_interface',
        templateUrl : 'js/app/imei_login_interface/imei_login_interface.html',
        // resolve : {
            // deps : ['uiLoad',
            // function(uiLoad) {
                // return uiLoad.load(['js/app/login_interface13/tu11_15.css']);
            // }]
        // }
    }).state('imei.imei_sign_in', {
        url : '/imei_sign_in',
        templateUrl : 'js/app/imei_sign_in/imei_sign_in.html',
        // resolve : {
            // deps : ['uiLoad',
            // function(uiLoad) {
                // return uiLoad.load(['js/app/sign_in14/tu11_15.css']);
            // }]
        // }
    }).state('imei.imei_email_signin', {
        url : '/imei_email_signin',
        templateUrl : 'js/app/imei_email_signin/imei_email_signin.html',
        // resolve : {
            // deps : ['uiLoad',
            // function(uiLoad) {
                // return uiLoad.load(['js/app/email_signin15/tu11_15.css']);
            // }]
        // }
    }).state('imei.imei_revamp_data', {
        url : '/imei_revamp_data',
        templateUrl : 'js/app/imei_revamp_data/imei_revamp_data.html',
        // resolve : {
            // deps : ['uiLoad',
            // function(uiLoad) {
                // return uiLoad.load(['js/app/revamp_data16/tu16.css']);
            // }]
        // }
    }).state('imei.imei_fans', {
        url : '/imei_fans',
        templateUrl : 'js/app/imei_fans/imei_fans.html',
        // resolve : {
            // deps : ['uiLoad',
            // function(uiLoad) {
                // return uiLoad.load(['js/app/fans19/tu19_22.css']);
            // }]
        // }
    }).state('imei.imei_person_center', {
        url : '/imei_person_center',
        templateUrl : 'js/app/imei_person_center/imei_person_center.html',
        // resolve : {
            // deps : ['uiLoad',
            // function(uiLoad) {
                // return uiLoad.load(['js/app/person_center20/tu19_22.css']);
            // }]
        // }
    }).state('imei.imei_revamp_password', {
        url : '/imei_revamp_password',
        templateUrl : 'js/app/imei_revamp_password/imei_revamp_password.html',
        // resolve : {
            // deps : ['uiLoad',
            // function(uiLoad) {
                // return uiLoad.load(['js/app/revamp_password21/tu11_15.css']);
            // }]
        // }
    }).state('imei.imei_home_imei', {
        url : '/imei_home_imei',
        templateUrl : 'js/app/imei_home_imei/imei_home_imei.html',
        // resolve : {
            // deps : ['uiLoad',
            // function(uiLoad) {
                // return uiLoad.load(['js/app/home23_imei/tu23.css']);
            // }]
        // }
    }).state('imei.imei_push', {
        url : '/imei_push',
        templateUrl : 'js/app/imei_push/imei_push.html',
        // resolve : {
            // deps : ['uiLoad',
            // function(uiLoad) {
                // return uiLoad.load(['js/app/push23/tu23.css']);
            // }]
        // }
    }).state('imei.imei_home_feedback', {
        url : '/imei_home_feedback',
        templateUrl : 'js/app/imei_home_feedback/imei_home_feedback.html',
        // resolve : {
            // deps : ['uiLoad',
            // function(uiLoad) {
                // return uiLoad.load(['js/app/home23_feedback/tu23.css',]);
            // }]
        // }
    })

    
    
    // .state('incubatorMobile.dashboard-v2', {
        // url : '/dashboard-v2',
        // templateUrl : 'tpl/blank.html'
    // })
        //这里：就是说把dashboar－v1的html注入到app的ui－view里面
    .state('incubatorMobile.dashboard-v1', {
        url : '/dashboard-v1',
        templateUrl : 'tpl/blank.html'
    })
    .state('incubatorMobile.dashboard-v2', {
        url : '/dashboard-v2',
        templateUrl : 'tpl/blank2.html'
    })
    ;
    //------------------------------------------app_model------------------------
     $stateProvider.state('imeimodel', {
        abstract : true,
        url : '/imeimodel',
        templateUrl : 'tpl/appmodel.html'
    })
   
    .state('imeimodel.index', {
        url : '/index',
        templateUrl : 'js/appmodel/home/home.html',
         
    }).state('imeimodel.imei_record', {
        url : '/imei_record',
        templateUrl : 'js/appmodel/imei_record/imei_record.html'
    }).state('imeimodel.imei_message', {
        url : '/imei_message',
        templateUrl : 'js/appmodel/imei_message/imei_message.html',
        // resolve : {
            // deps : ['uiLoad',
            // function(uiLoad) {
                // return uiLoad.load(['js/appmodel/message8/tu8.css']);
            // }]
        // }
    }).state('imeimodel.imei_not_login', {
        url : '/imei_not_login',
        templateUrl : 'js/appmodel/imei_not_login/imei_not_login.html',
        // resolve : {
            // deps : ['uiLoad',
            // function(uiLoad) {
                // return uiLoad.load(['js/appmodel/not_login9/tu9.css']);
            // }]
        // }
    }).state('imeimodel.imei_login_status', {
        url : '/imei_login_status',
        templateUrl : 'js/appmodel/imei_login_status/imei_login_status.html',
        // resolve : {
            // deps : ['uiLoad',
            // function(uiLoad) {
                // return uiLoad.load(['js/appmodel/login_status10/tu9.css']);
            // }]
        // }
    }).state('imeimodel.imei_homestart', {
        url : '/imei_homestart',
        templateUrl : 'js/appmodel/imei_homestart/imei_homestart.html',
        // resolve : {
            // deps : ['uiLoad',
            // function(uiLoad) {
                // return uiLoad.load(['js/appmodel/homestart/start.css']);
            // }]
        // }
    });
}]);
